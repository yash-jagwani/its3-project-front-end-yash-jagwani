import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Book } from 'src/app/model/Book';
import { BooksService } from 'src/app/service/books.service';
import { TokenStorageService } from 'src/app/service/token-storage.service';

@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.scss']
})
export class BookListComponent implements OnInit {

  books : Observable<Book[]>;
  private roles: string[];
  isLoggedIn = false;
  showAdminBoard = false;
  username: string;

  book: any;
  currentBook = null;
  currentIndex = -1;
  author = '';

  constructor(private bookService : BooksService, private tokenStorageService: TokenStorageService) { }
  
  ngOnInit(): void {
    this.isLoggedIn = !!this.tokenStorageService.getToken();
    this.books = this.bookService.getBooks();

    if (this.isLoggedIn) {
      const user = this.tokenStorageService.getUser();
      this.roles = user.roles;

      this.showAdminBoard = this.roles.includes('ROLE_ADMIN');

      this.username = user.username;
    }
    //this.books.subscribe;
  }

  retrieveBooks(): void {
    this.bookService.getBooks()
      .subscribe(
        data => {
          this.book = data;
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }

  refreshList(): void {
    this.retrieveBooks();
    this.currentBook = null;
    this.currentIndex = -1;
  }

  setActiveBook(book, index): void {
    this.currentBook = book;
    this.currentIndex = index;
  }

  removeAllBooks(): void {
    this.bookService.deleteAll()
      .subscribe(
        response => {
          console.log(response);
          this.retrieveBooks();
        },
        error => {
          console.log(error);
        });
  }

  searchAuthor(): void {
    this.bookService.findByAuthor(this.author)
      .subscribe(
        data => {
          this.book = data;
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }

}
