import { Component, OnDestroy, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ChatMessageDTO } from 'src/app/model/ChatMessageDTO';
import { WebSocketService } from 'src/app/service/web-socket.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit, OnDestroy {
  
  constructor(public webSocketService: WebSocketService) { }

  ngOnInit(): void {
    this.webSocketService.openWebSocket();
  }

  ngOnDestroy(): void {
    this.webSocketService.closeWebSocket();
  }

  sendMessage(sendForm: NgForm) {
    const chatMessageDTO = new ChatMessageDTO(sendForm.value.user, sendForm.value.message);
    this.webSocketService.sendMessage(chatMessageDTO);
    sendForm.controls.message.reset();
  }

}
