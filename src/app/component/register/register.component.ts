import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/service/auth.service';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  form: any = {};
  isSuccessful = false;
  isSignUpFailed = false;
  errorMessage = '';

  constructor(private authService: AuthService) { }

  ngOnInit(): void {
  }

  onSubmit(): void {
    this.authService.register(this.form).subscribe(
      data => {
        console.log(data);
        this.isSuccessful = true;
        this.isSignUpFailed = false;
      },
      err => {
        this.errorMessage = err.error.message;
        this.isSignUpFailed = true;
      }
    );
  }

}

// import { Component, OnInit } from '@angular/core';
// import { Router } from '@angular/router';
// import { FormBuilder, FormGroup, Validators } from '@angular/forms';
// import { first } from 'rxjs/operators';

// import { AlertService} from 'src/app/service/alert.service';
// import { AuthenticationService } from 'src/app/service/authentication.service';
// import { UsersService } from 'src/app/service/users.service';

// @Component({
//   selector: 'app-register',
//   templateUrl: './register.component.html',
//   styleUrls: ['./register.component.scss']
// })
// export class RegisterComponent implements OnInit {
//   registerForm: FormGroup;
//   loading = false;
//   submitted = false;

//   constructor(
//       private formBuilder: FormBuilder,
//       private router: Router,
//       private authenticationService: AuthenticationService,
//       private userService: UsersService,
//       private alertService: AlertService
//   ) {
//       // redirect to home if already logged in
//       if (this.authenticationService.currentUserValue) {
//           this.router.navigate(['/']);
//       }
//   }

//   ngOnInit() {
//       this.registerForm = this.formBuilder.group({
//           firstName: ['', Validators.required],
//           lastName: ['', Validators.required],
//           username: ['', Validators.required],
//           password: ['', [Validators.required, Validators.minLength(6)]]
//       });
//   }

//   // convenience getter for easy access to form fields
//   get f() { return this.registerForm.controls; }

//   onSubmit() {
//       this.submitted = true;

//       // reset alerts on submit
//       this.alertService.clear();

//       // stop here if form is invalid
//       if (this.registerForm.invalid) {
//           return;
//       }

//       this.loading = true;
//       this.userService.register(this.registerForm.value)
//           .pipe(first())
//           .subscribe(
//               data => {
//                   this.alertService.success('Registration successful', true);
//                   this.router.navigate(['/login']);
//               },
//               error => {
//                   this.alertService.error(error);
//                   this.loading = false;
//               });
//   }
// }
