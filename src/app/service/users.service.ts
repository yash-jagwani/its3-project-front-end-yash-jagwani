import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

const API_URL = 'http://localhost:8080/api/test/';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private http: HttpClient) { }

  getPublicContent(): Observable<any> {
    return this.http.get(API_URL + 'all', { responseType: 'text' });
  }

  getUserBoard(): Observable<any> {
    return this.http.get(API_URL + 'user', { responseType: 'text' });
  }

  getAdminBoard(): Observable<any> {
    return this.http.get(API_URL + 'admin', { responseType: 'text' });
  }
}

// import { Injectable } from '@angular/core';
// import { HttpClient } from '@angular/common/http';

// import { User } from 'src/app/model/User';

// @Injectable({
//   providedIn: 'root'
// })
// export class UsersService {
//   constructor(private http: HttpClient) { }

//   getAll() {
//       return this.http.get<User[]>(`/users`);
//   }

//   register(user: User) {
//       return this.http.post(`/users/register`, user);
//   }

//   delete(id: number) {
//       return this.http.delete(`/users/${id}`);
//   }
// }
